import fs = require('fs');

/* Variables declaration */
const mower1: Mower = {
  x: 0,
  y: 0,
  orientation: '',
  path: '',
};

const mower2: Mower = {
  x: 0,
  y: 0,
  orientation: '',
  path: '',
};

const mapSize: Map = {
  x: 0,
  y: 0,
};

const cardinalPoints = ['N', 'E', 'S', 'W'];

/**
 * Read the file and extract the required data
 */
fs.readFile('./init.txt', 'utf8', (err, extractedData) => {
  if (err)
    return console.log(err);

  const mowerData = extractedData.split('\n');

  // Remove the automated trailing line if existing
  if (mowerData[mowerData.length - 1] === '')
    mowerData.pop();

  mapSize.x = Number(mowerData[0].split(' ')[0]);
  mapSize.y = Number(mowerData[0].split(' ')[1]);

  mower1.x = Number(mowerData[1].split(' ')[0]);
  mower1.y = Number(mowerData[1].split(' ')[1]);
  mower1.orientation = mowerData[1].split(' ')[2];
  mower1.path = mowerData[2];

  mower2.x = Number(mowerData[3].split(' ')[0]);
  mower2.y = Number(mowerData[3].split(' ')[1]);
  mower2.orientation = mowerData[3].split(' ')[2];
  mower2.path = mowerData[4];

  // Starting the two mowers
  mowingTheLawn(mower1);
  mowingTheLawn(mower2);
});

/**
 * Handle the orientation of the mower
 */
function turningTheMower90deg(orientation: string, action: string): string {
  switch(action) {
    case 'R':
      return (cardinalPoints.indexOf(orientation) < cardinalPoints.length - 1) ? cardinalPoints[cardinalPoints.indexOf(orientation) + 1] : cardinalPoints[0];
    case 'L':
      return (cardinalPoints.indexOf(orientation) > 0) ? cardinalPoints[cardinalPoints.indexOf(orientation) - 1] : cardinalPoints[cardinalPoints.length - 1];
  };
}

/**
 * Handle the movement of the mower
 */
function moveTheMower(mower: Mower): void {
  switch(mower.orientation) {
    case 'N':
      if (mower.y < mapSize.y)
        mower.y++;
    break;
    case 'E':
      if (mower.x < mapSize.x)
        mower.x++;
    break;
    case 'S':
      if (mower.y > 0)
        mower.y--;
    break;
    case 'W':
      if (mower.x > 0)
        mower.x--;
    break;
  };
}

/**
 * Run the mower's sequence
 */
function mowingTheLawn(mower: Mower): void {
  const action = mower.path.substring(0, 1);

  if (action === 'R' || action === 'L')
    mower.orientation = turningTheMower90deg(mower.orientation, action);
  else if (action === 'F')
    moveTheMower(mower);

  // Recursively mowing the lawn
  if (mower.path.length > 0) {
    mower.path = mower.path.substring(1);
    mowingTheLawn(mower);
  } else {
    console.log(`${mower.x} ${mower.y} ${mower.orientation}`);
  }
};

/* Type definition */
type Mower = {
  x: number,
  y: number,
  orientation: string,
  path: string,
}

type Map = {
  x: number,
  y: number,
}
